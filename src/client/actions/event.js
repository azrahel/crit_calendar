import Type from './types/event'

import { 
  getEvents
} from '../dbHandlers/events'

function fetchAllEventsAction(eventsArray) {
  return {
    type: Type.GET_ALL_EVENTS,
    data: eventsArray
  }
}

export function fetchAllEvents(startDate, endDate) {
  return (dispatch) => {
    getEvents(
      startDate, 
      endDate
    ).then((events) => {
      dispatch(fetchAllEventsAction(events))
    })
  }
}
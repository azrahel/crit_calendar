import {
  fetchAllEvents,
}  from './event.js'

import {
  setFilterAction
}  from './areaFilters.js'

export default {
  fetchAllEvents,
  setFilterAction
}
import Type from './types/areaFilters'

export function setAreaFilterAction(areaFilterData) {
  return {
    type: Type.SET_EVENTS_AREA_FILTER,
    data: areaFilterData
  }
}
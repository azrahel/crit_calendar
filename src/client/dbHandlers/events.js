import hyperquest from 'hyperquest';
import 'whatwg-fetch'

//move to config
let baseUrl = 'http://localhost:3001'
let apiVersion = '/v1'

let baseGetFromURL = baseUrl + apiVersion;

export function getEvents(startDate, endDate) {
  let getFromURL = baseGetFromURL + '/events';
  
  if(startDate && endDate) {
    getFromURL = getFromURL + 
      '?startDate=' + startDate + 
      '&endDate=' + endDate
  }

  return fetch(getFromURL, {
    method: 'GET'
  }).then((response) => {
    // raw response is passed to first 'then' 
    return response.json()
  }).then((json) => {
    // whatever you return from first then is passed here (json in this case)
    return json
  }).catch((error) => {
    console.log('CATCHED ERROR WHILE HANDLING JSON RESPONSE')
    console.log(error)
  })
}

export function getEventsCount(continent, country) {
  let getFromURL = `${baseGetFromURL}/count/events/`;
  
  if(continent) {
    getFromURL = `${getFromURL}${continent}/`
  } if(country) {
    getFromURL = `${getFromURL}${country}`
  }

  return fetch(getFromURL, {
    method: 'GET'
  }).then((response) => {
    // raw response is passed to first 'then' 
    return response.json()
  }).then((json) => {
    // whatever you return from first then is passed here (json in this case)
    return json
  }).catch((error) => {
    console.log('CATCHED ERROR WHILE HANDLING JSON RESPONSE')
    console.log(error)
  })
}

export function getEventsForContinent(continent, startDate, endDate) {
  let getFromURL = baseGetFromURL + 
    '/events/continent/' + continent;
  
  if(startDate && endDate) {
    getFromURL = getFromURL + 
      '?startDate=' + startDate + 
      '&endDate=' + endDate
  }

  return fetch(getFromURL, {
    method: 'GET'
  }).then((response) => {
    // raw response is passed to first 'then' 
    return response.json()
  }).then((json) => {
    // whatever you return from first then is passed here (json in this case)
    return json
  }).catch((error) => {
    console.log('CATCHED ERROR WHILE HANDLING JSON RESPONSE')
    console.log(error)
  })
}

export function getEventsForCountry(continent, country, startDate, endDate) {
  let getFromURL =  baseGetFromURL + 
    '/events/continent/'+ continent + 
    '/country/' + country;
  
  if(startDate && endDate) {
    getFromURL = getFromURL + 
      '?startDate=' + startDate + 
      '&endDate=' + endDate
  }

  return hyperquest.get(getFromURL);
}

export function getEventsForCity(continent, country, city, startDate, endDate) {
  let getFromURL = baseGetFromURL + 
    '/events/continent/' + continent + 
    '/country/' + country + 
    '/city' + city;
  
  if(startDate && endDate) {
    getFromURL = getFromURL + 
      '?startDate=' + startDate + 
      '&endDate=' + endDate
  }

  return hyperquest.get(getFromURL);
}


// trzeba ten cały FETCH POST JSON w jedną metodę upakować, żby nie powtarzać wszędzie 
// <3
export function addEvent(event) {
  let body = JSON.stringify(event)

  return fetch(baseGetFromURL + '/events/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: body
  }).then((response) => {
    // raw response is passed to first 'then' 
    return response.json()
  }).then((json) => {
    // whatever you return from first then is passed here (json in this case)
    return json
  }).catch((error) => {
    console.log('CATCHED ERROR WHILE HANDLING JSON RESPONSE')
  })
}



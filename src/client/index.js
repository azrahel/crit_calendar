import React from 'react'
import ReactDOM from 'react-dom'
import createLogger from 'redux-logger'
import { createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import initialState from './initialState'
import eventsCalendar from './reducers/index'

import styles from './styles/index.scss'
import styles1 from 'react-toolbox/lib/dropdown';

import Calendar from './components/Calendar.jsx';

const loggerMiddleware = createLogger()

const store = createStore(
    eventsCalendar,
    initialState,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
);

const history = syncHistoryWithStore(browserHistory, store)

const rootRoute = {
  component: 'div',
  childRoutes: [{
    path: '/',
    component: require('./components/App').default,
    childRoutes: [
      require('./routes/User/Settings'),
      require('./routes/Continent'),
      require('./routes/Year')
    ]
  }]
}

ReactDOM.render(
  <Provider store = {store}>
    <Router history = {history} routes = {rootRoute} />
  </Provider>,
  document.getElementById('root')
);


import Type from '../actions/types/areaFilters'


//todo: separate initial state into partials and pass them here also in reducers
//as default value

export default function areaFilters(state = {}, action) {
  switch(action.type) {
    case Type.SET_EVENTS_AREA_FILTER:
      return Object.assign({}, state, action.data)
      break
  }

  return state
}

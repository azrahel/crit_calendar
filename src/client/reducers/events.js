import Type from '../actions/types/event'

export default function events(state = [], action) {
  //TODO 2 - AND THAN:
  // case ADD_TODO:
  // case TOGGLE_TODO:
  //   return Object.assign({}, state, {
  //     todos: todos(state.todos, action)
  //   })
  // default:
  //   return state

  //TODO 3 - your main reducer can look like this: (!!)
  // function todoApp(state = {}, action) {
  //   return {
  //     visibilityFilter: visibilityFilter(state.visibilityFilter, action),
  //     todos: todos(state.todos, action)
  //   }
  // }

  //TODO 4 - use COMBINE REDUCERS function to do just the same, but cleaner
  // import { combineReducers } from 'redux'

  // const todoApp = combineReducers({
  //   visibilityFilter,
  //   todos
  // })

  // export default todoApp
  // Note that this is equivalent to:

  // export default function todoApp(state = {}, action) {
  //   return {
  //     visibilityFilter: visibilityFilter(state.visibilityFilter, action),
  //     todos: todos(state.todos, action)
  //   }
  // }

  // TODO 5
  // Because combineReducers expects an object, we can put all top-level reducers into a separate file, export each reducer function, and use import * as reducers to get them as an object with their names as the keys:

  // import { combineReducers } from 'redux'
  // import * as reducers from './reducers'

  // const todoApp = combineReducers(reducers)
  // Because import * is still new syntax, we don't use it anymore in the documentation to avoid confusion, but you may encounter it in some community examples.

  switch(action.type) {
    case Type.GET_ALL_EVENTS:
      return action.data
      break
    case Type.GET_CONTINENT_EVENTS:
      return action.data
      break
    case Type.GET_EVENTS_COUNT: 
      console.log('action.data')
      console.log(action.data)
      console.log(Object.assign({ ...state }, action.data))
      
      return Object.assign({ ...state }, action.data)
      break
  }

  return state
}
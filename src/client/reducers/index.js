import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import events from './events'
import areaFilters from './areaFilters'

const eventsCalendar = combineReducers({
  areaFilters,
  events,
  routing: routerReducer
})

export default eventsCalendar
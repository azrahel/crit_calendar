export function formatDBEventToCalendarEvent(dbEvent) {
  return {
    allDay: true,
    title: dbEvent.name,
    start: new Date(dbEvent.ISOStartDate),
    end: new Date(dbEvent.ISOEndDate),
    description: dbEvent.description
  }
}
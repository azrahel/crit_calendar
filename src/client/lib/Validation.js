import validator from 'validator'

export default {
  stringInputValid(value, errorText, required = false) {
    const validationObject = { success: true, text: '' }

    if ((value === undefined || value === null || String(value).length === 0) && required) {
      validationObject.success = false
      validationObject.text = errorText || 'This field is required.'
    }

    return validationObject
  },

  checkboxesValid(boolsLiteral, errorText, required = false) {
    const validationObject = { success: true, text: '' }
    let checked = false;

    for(let key in boolsLiteral) {
      if(key !== 'validation' && boolsLiteral[key]) {
        checked = true
      }
    }

    if(!checked) {
      validationObject.success = false
      validationObject.text = 'Choose at least one option.'
    }

    return validationObject
  },

  loginPasswordInputValidation(password) {
    if (password.length < 1) {
      return {
        success: false,
        text: 'Please enter password.'
      }
    }

    return {
      success: true
    }
  },

  passwordInputValidation(password) {
    if (password.length < 1) {
      return {
        success: false,
        text: 'Please enter password.'
      }
    }

    if (password.length < 8) {
      return {
        success: false,
        text: 'Password must have at least 8 characters.'
      }
    }

    return {
      success: true
    }
  },

  tokenValidation(token) {
    if (!token) {
      return {
        success: false,
        text: 'Token is empty.'
      }
    }
    return {
      success: true
    }
  },

  repeatPasswordInputValidation(password, repeatPassword) {
    if (password !== repeatPassword && password.length >= 6) {
      return {
        success: false,
        text: 'Password and repeat password must be the same.'
      }
    }

    return {
      success: true
    }
  },

  currentPositionValidation(position) {
    return (position.length > 0)
  },

  emailInputValidation(email) {
    let validation

    if (validator.isEmail(email)) {
      validation = {
        success: true
      }
    } else {
      validation = {
        success: false,
        text: 'Please enter correct email.'
      }
    }

    return validation
  },

  emailRepetitionInputValidation(email, repetition) {
    let validation

    if (email === repetition) {
      validation = {
        success: true
      }
    } else {
      validation = {
        success: false,
        text: 'E-mails must match.'
      }
    }

    return validation
  },

  phoneNumberInputValidation(phone, errorText) {
    if (String(phone).length >= 9
        && String(phone).length <= 20
        && /^([0-9.+() -]*)$/.test(String(phone))
    ) {
      return {
        success: true
      }
    }

    return {
      success: false,
      text: errorText || 'Invalid phone number.'
    }
  },

  companiesAndCitiesValidation(id, name) {
    const firstCondition = (id !== 0)
    const secondCondition = (name.length > 0)

    // firstCondition XOR secondCondition
    return (
      (firstCondition && !secondCondition) ||
      (!firstCondition && secondCondition)
    )
  },

  urlInputValid(url, errorText, required = false) {
    const validationObject = { success: true, text: '' }


    if (url.length > 0 && !validator.isURL(url)) {
      validationObject.success = false
      validationObject.text = errorText || 'Please enter valid URL.'
    }
    if (url.length === 0 && required) {
      validationObject.success = false
      validationObject.text = errorText || 'This field is required.'
    }

    return validationObject
  },

  nameSurnameValid(nameSurname, errorText, required = false) {
    const validationObject = { success: true, text: '' }

    if (nameSurname.length > 0 && nameSurname.split(' ').length < 2) {
      validationObject.success = false
      validationObject.text = errorText ||
                              'Name and surname should be at least 2 words.'
    }
    if (nameSurname.split(' ').length >= 2) {
      if (nameSurname.split(' ').some(
        nameSurnamePart => (
          nameSurnamePart.length === 0
        ))
      ) {
        validationObject.success = false
        validationObject.text = errorText || 'Please use only nonempty words.'
      }
    }
    if (nameSurname.length === 0 && required) {
      validationObject.success = false
      validationObject.text = errorText || 'This field is required.'
    }

    return validationObject
  },

  multiselectValid(valuesArray, errorText, required = false) {
    const validationObject = { success: true, text: '' }

    if (valuesArray.length === 0 && required) {
      validationObject.success = false
      validationObject.text = errorText ||
                              'Choose at least one option.'
    }

    return validationObject
  },

  numberValid(value, errorText, required = false) {
    function isNumeric(num) {
      return !isNaN(num)
    }

    const validationObject = { success: true, text: '' }

    if (value.length === 0 && required) {
      validationObject.success = false
      validationObject.text = errorText || 'This field is required.'
    }
    if (!isNumeric(value)) {
      validationObject.success = false
      validationObject.text = errorText || 'Value must be numeric.'
    }

    return validationObject
  },

  radioButtonsValid(radiosStates, errorText) {
    const validationObject = { success: true, text: '' }

    if (radiosStates.filter(radioState => (radioState === true)).length === 0) {
      validationObject.success = false
      validationObject.text = errorText || 'Please choose one option.'
    }

    return validationObject
  }
}

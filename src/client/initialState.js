export default {
  events: {
    AF: {
      //each continents holds very similar objects of countries
      //ex 
      // poland: {
      //  eventsQuantity: 12
      //  warsaw: {
      //    eventsQuantity: 2
      //    events: []
      //  }
      //  gdansk: {
      //    eventsQuantity: 10
      //    events: []
      //  }
      // },
      // france: { eventsQuantity: ..., $cityName: {}, $cityName: {} } 
      // etc
      // etc
      eventsQuantity: 0
    },
    AS: {
      eventsQuantity: 0
    },
    EU: {
      eventsQuantity: 0
    },
    NA: {
      eventsQuantity: 0
    },
    OC: {
      eventsQuantity: 0
    },
    SA: {
      eventsQuantity: 0
    }
  },
  areaFilters: {
    continent: '',
    country: '',
    city: ''
  }
}
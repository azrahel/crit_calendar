import React, { Component } from 'react';
import { getEventsForContinent } from '../../../dbHandlers/events';
import { formatDBEventToCalendarEvent } from '../../../lib/functions';

import Calendar from '../../../components/Calendar.jsx';
import List from '../../../components/List.jsx';

export default class Country extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      events: null, 
      startDate: null, 
      endDate: null 
    };
  }

  renderBody(params) {
    let body;
    // if(Object.keys(this.props.location.query).length > 0) {
      // let timeSpan = Object.keys(this.props.location.query)[0];
      
      body = <List 
        // timeSpan = { timeSpan }
        area = { { continent: this.props.params.continent } }
      />
    // }
    // else {
    //   body = <Calendar 
    //     area = { { continent: this.props.params.continent } }
    //     startDate = { this.props.location.query.startDate }
    //     endDate = { this.props.location.query.endDate }
    //   />;
    // }
    
    return body;
  }

  render() {
    let { children, params } = this.props
    console.log('rendering continent?')

    return (
      <div>
        <div>{ children || this.renderBody(params) }</div>
      </div>
    );
  }
}

module.exports = {
  path: ':month',
  
  getChildRoutes(partialNextState, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('../Day'),
      ])
    });
  },

  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/Month.jsx').default)
    });
  }
}
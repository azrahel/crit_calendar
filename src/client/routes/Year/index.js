module.exports = {
  path: 'year/:year',

  getChildRoutes(partialNextState, cb) {
    require.ensure([], (require) => {
      cb(null, [
        require('../Month'),
      ])
    })
  },

  getComponent(nextState, cb) {

    require.ensure([], (require) => {
      cb(null, require('./components/Year.jsx').default)
    })
  },
}
import React, { Component } from 'react';
import { getEventsForContinent } from '../../../dbHandlers/events';
import { formatDBEventToCalendarEvent } from '../../../lib/functions';

import Calendar from '../../../components/Calendar.jsx';

export default class Year extends Component {
  constructor(props) {
    super(props);
    this.state = { events: null }
  }

  componentWillMount() {
    getEventsForContinent(
      this.props.params.continent, 
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events),
      });
    }.bind(this));
  }

  renderBody(params) {
    let calendar = null;

    if(this.state.events) {
      let calendarEvents = this.state.events.map(function(event) {
        return formatDBEventToCalendarEvent(event);
      })

      calendar = <Calendar 
        events = { calendarEvents }
        startDate = { this.state.startDate }
        endDate = { this.state.endDate }
      />;
    }
    
    return calendar;
  }

  render() {
    let { children, params } = this.props

    return (
      <div>
        <div>{ children || this.renderBody(params) }</div>
      </div>
    );
  }
}

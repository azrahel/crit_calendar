module.exports = {
  path: ':day',

  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/day.jsx').default)
    })
  }
}
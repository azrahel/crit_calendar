import React, { Component } from 'react';

export default class Country extends Component {
  componentDidMount() {
  }

  renderBody(params) {
    return <div>
      { 'Continent: ' + params.continent  }
      { ', Country: ' + params.country }
    </div>;
  }

  render() {
    let { children, params } = this.props
    
    return (
      <div>{ children || this.renderBody(params) }</div>
    );
  }
}

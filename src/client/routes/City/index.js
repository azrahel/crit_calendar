module.exports = {
  path: ':city',
  
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/City.jsx').default)
    })
  }
}
import React, { Component } from 'react';

export default class City extends Component {
  componentDidMount() {
    let { children, params } = this.props
  }

  renderBody(params) {
    return <div>
      { 'Continent: ' + params.continent  }
      { ', Country: ' + params.country }
      { ', City: ' + params.city }
    </div>;
  }

  render() {
    let { children, params } = this.props
    
    return (
      <div>{ children || this.renderBody(params) }</div>
    );
  }
}

module.exports = {
  path: 'settings',
  onEnter: () => {
    console.log('entering settings!')
  },
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/Settings.jsx').default)
    })
  }
}
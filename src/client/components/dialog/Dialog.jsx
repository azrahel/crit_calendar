import React, { Component } from 'react'

import Dialog from 'react-toolbox/lib/dialog'
import Input  from 'react-toolbox/lib/input'

import { addEvent } from 'dbHandlers/events'
import Validation   from '../../lib/Validation'
import cities       from 'countries-cities'

import style          from './style.scss'

import GeoSection   from './Geosection'
import RaceFormats  from './RaceFormats'
import Datepicker   from './Datepicker'
import BicycleTypes from './BicycleTypes'

import _ from 'lodash'

const OTHER = 'Other'

let initialState = {
  name:           { value: '', validation: { text: '', success: null } },
  startDate:      { value: '', validation: { text: '', success: null } },
  distance:       { value: '', validation: { text: '', success: null } },
  continent:      { value: '', validation: { text: '', success: null } },
  country:        { value: '', validation: { text: '', success: null } },
  city:           { value: '', validation: { text: '', success: null } },
  organizer:      { value: '', validation: { text: '', success: null } },
  url:            { value: '', validation: { text: '', success: null } },
  customCategory: { value: '', validation: { text: '', success: null } },
  customFormat:   { value: '', validation: { text: '', success: null } },
  endDate:        { value: '' },
  provideCustomCity: false,
  categories: {
    Road:         false,
    MTB:          false,
    TRI:          false,
    Track:        false,
    'Fat bike':   false,
    'Fixed gear': false,
    CX:           false,
    Trial:        false,
    BMX:          false,
    Horizontal:   false,
    [OTHER]:      false,
    validation:   { text: '', success: null }
  },
  formats: {
    Criterium: true,
    Indoor:    false,
    Stage:     false,
    Race:      false,
    Marathon:  false,
    Brevet:    false,
    Tour:      false,
    '12h':     false,
    '24h':     false,
    [OTHER]:   false,
    validation: { value: '', validation: { text: '', success: null } }
  }
}

export default class EventDialog extends Component {
  constructor(props) {
    super(props);

    this.addEvent         = this.addEvent.bind(this)
    this.onChange         = this.onChange.bind(this)
    this.getActiveFormat  = this.getActiveFormat.bind(this)
    this.fieldValid       = this.fieldValid.bind(this)

    this.state = _.clone(initialState, true)
  }

  onChange(name, value = '', label) {
    if(name === 'startDate') {
      this.fieldValid(name, value)
    }

    console.log(name, value, label)

    this.setState({ ...this.state, [name]: { ...this.state[name], value, label } })
  }

  addEvent() {
    function formatDialogEventForDB(event) {
      let dbEvent       = {}
      let categories    = []
      let chosenFormat

      for(let category in event.categories) {
        if(category !== OTHER && category !== 'validation' && event.categories[category]) {
          categories.push(category)
        }
      }

      if(event.formats[OTHER]) {
        chosenFormat = event.customFormat.value
      } else {
        for(let format in event.formats) {
          if(format !== OTHER && format !== 'validation' && event.formats[format]) {
            chosenFormat = format
          }
        }
      }

      dbEvent.name            = event.name.value
      dbEvent.distance        = event.distance.value
      dbEvent.continent       = event.continent.value.value
      dbEvent.country         = event.country.value.label
      dbEvent.city            = event.city.value.label
      dbEvent.url             = event.url.value
      dbEvent.ISOStartDate    = new Date(event.startDate.value)
      dbEvent.ISOEndDate      = new Date(event.endDate.value || event.startDate.value)
      dbEvent.categories      = categories
      
      if(event.categories[OTHER]) {
        dbEvent.customCategory  = event.customCategory.value
      }
      
      dbEvent.format          = chosenFormat
      dbEvent.organizer       = event.organizer.value
      //TODO trzeba dodać moderated do wszystkich eventów
      dbEvent.moderated       = false

      return dbEvent
    }

    if(this.formValid()) {
      addEvent(formatDialogEventForDB(this.state)).then((response) => {

        // TODO -  do zmiany. trzeba tylko response.success sprawdzać (true/false)
        if(response.success) {
          alert(response.value)
        } else {
          this.setState(_.clone(initialState, true))
          this.props.toggleDialog()   
        }
      })  
    }
  }

  onCustomCityCheckboxChange() {
    this.setState({
      provideCustomCity: !this.state.provideCustomCity,
      city: { validation: { text: '', success: null } }
    })
  }

  formValid() {
    let doNotValidate = ['provideCustomCity', 'endDate', 'formats']
    let valid = true;

    for(let field in this.state) {
      let value = field !== 'categories' && field !== 'formats'
        ? this.state[field].value
        : this.state[field]

      if(doNotValidate.indexOf(field) < 0) {
        if((field === 'customCategory' && this.state.categories[OTHER]) ||
           (field === 'customFormat' && this.state.formats[OTHER])
        ) {
          if(!this.fieldValid(field, value)) {
            valid = false
          } 
        } else {
          if(field !== 'customFormat' && field !== 'customCategory') {
            if(!this.fieldValid(field, value)) {
              valid = false
            } 
          }
        }
      }
    }

    return valid
  }

  fieldValid(name, value) {
    let validation = {}

    switch(true) {
      case  name === 'continent'      || name === 'country'       ||
            name === 'city'           || name === 'organizer'     ||
            name === 'customCategory' || name === 'customFormat'  ||
            name === 'startDate'      || name === 'name':

        validation = Validation.stringInputValid(value, '', true)

        break
      case name === 'category':
        validation = Validation.nameSurnameValid(value, '', true)

        break
      case name === 'url':
        validation = Validation.urlInputValid(value, '', true)

        break
      case name === 'categories':
        validation = Validation.checkboxesValid(value, '', true)

        break
      case name === 'distance':
        validation = Validation.numberValid(value, '', true)

        break
      default: 

        break
    }

    let state = this.state
    state[name].validation = validation

    this.setState(state)

    return validation.success
  }

  onCategoryChange(name, value) {
    let categories = this.state.categories
    let customCategory = this.state.customCategory

    if(name === OTHER && !value) {
      customCategory = { value: '', validation: { text: '', success: null } }
    }

    categories[name] = value;

    this.setState({ ...this.state, categories, customCategory })
  }


  onFormatChange(data) {
    let formats = data.formats
    let customFormat = this.state.customFormat

    if(name !== OTHER) {
      customFormat = { value: '', validation: { text: '', success: null } }
    }

    this.setState({ ...this.state, formats, customFormat })
  }

   getActiveFormat() {
    for(let format in this.state.formats) {
      if(format !== 'validation' && this.state.formats[format]) {
        return format
      }
    }

    return format
  }

  render() {
    return <Dialog
      className       = { style.addEventDialog }
      actions         = { [
        { label: 'Cancel',  onClick: this.props.toggleDialog },
        { label: 'Save',    onClick: this.addEvent }
      ] }
      active          = { this.props.active }
      onEscKeyDown    = { this.props.toggleDialog }
      onOverlayClick  = { this.props.toggleDialog }
      title           = 'Add event'
    >
      <section>
        <span>Name</span>
        <Input
          type      = 'text'
          name      = 'name'
          value     = { this.state.name.value }
          error     = { this.state.name.validation.text }
          onBlur    = { () => { this.fieldValid('name', this.state.name.value) } }
          onChange  = { (value) => { this.onChange('name', value) } }
          maxLength = { 60 }
        />
      </section>
      <GeoSection
        continent         = { this.state.continent }
        country           = { this.state.country }
        city              = { this.state.city }
        provideCustomCity = { this.state.provideCustomCity }
        onChange          = { this.onChange }
        onBlur            = { (name) => { this.fieldValid(name, this.state[name].value) } }
        onCustomCityCheckboxChange = {
          () => {
            this.onCustomCityCheckboxChange()
          }
        }
      />
      <BicycleTypes
        categories        = { this.state.categories }
        customCategory    = { this.state.customCategory }
        onBlur            = { (name) => { this.fieldValid(name, this.state[name].value) } }
        onChange          = { (name, value) => { this.onChange(name, value) } }
        onCategoryChange  = { (name, value) => { this.onCategoryChange(name, value) } }
        OTHER             = { OTHER }
      />
      <RaceFormats
        formats         = { this.state.formats }
        customFormat    = { this.state.customFormat }
        onBlur          = { (name) => { this.fieldValid(name, this.state[name].value) } }
        onChange        = { (name, value) => { this.onChange(name, value) } } 
        onFormatChange  = { (data) => { this.onFormatChange(data) } }
        OTHER           = { OTHER }
      />
      <section>
        <div>
          <span>Distance</span> 
          {
            //TODO (user units HERE or checkboxes)
          }
          <Input 
            type      = 'text'
            name      = 'distance'
            value     = { this.state.distance.value }
            error     = { this.state.distance.validation.text }
            onBlur    = { () => { this.fieldValid('distance', this.state.distance.value) } }
            type      = 'number'
            onChange  = { (value) => { this.onChange('distance', value) } }
          />
        </div>
        <div>
          <span>Organizer</span>
          <Input 
            type      = 'text'
            name      = 'organizer'
            value     = { this.state.organizer.value }
            error     = { this.state.organizer.validation.text }
            onBlur    = { () => { this.fieldValid('organizer', this.state.organizer.value) } }
            onChange  = { (value) => { this.onChange('organizer', value) } }
            maxLength = { 30 }
          />
        </div>
        <div>
          <span>Link to event page</span>
          <Input 
            type      = 'text'
            name      = 'url'
            value     = { this.state.url.value }
            error     = { this.state.url.validation.text }
            onBlur    = { () => { this.fieldValid('url', this.state.url.value) } }
            onChange  = { (value) => { this.onChange('url', value) } }
            maxLength = { 80 }
          />
        </div>
        <Datepicker
          startDate = { this.state.startDate }
          endDate   = { this.state.endDate }
          onBlur    = { (name) => { this.fieldValid(name, this.state[name].value) } }
          onChange  = { (name, value) => this.onChange(name, value) }
        />
      </section>
    </Dialog>
  }
}
import React, { Component } from 'react'

import Input from 'react-toolbox/lib/input'
import Checkbox from 'react-toolbox/lib/checkbox'
import validator from 'validator'

import style from './style.scss'
import checkboxStyle from 'styles/ui/checkboxTheme.scss'

export default class BicycleTypes extends Component {
  render() {
    let categories = []

    for(let category in this.props.categories) {
      if(category !== 'validation') {
        categories.push(
          <Checkbox
            key       = { category }
            theme     = { checkboxStyle }
            checked   = { this.props.categories[category] }
            label     = { category }
            onChange  = { (value) => {
              this.props.onCategoryChange(category, value) }
            }
          />
        )  
      }
    }

    return <section>
      <span>
        Select type(s) of bicycle allowed
        <span className = { style.selectError }>
          {
            !this.props.categories[this.props.OTHER]
              ? this.props.categories.validation.text
              : ''
          }
        </span>
      </span>
      {
        [
          categories,
          <Input
            key     = { 'customCategoryInput' }
            type    = 'text'
            hint    = 'provide your own type'
            name    = 'customCategory'
            onBlur  = { () => { this.props.onBlur('customCategory') } }
            error   = {
              this.props.categories[this.props.OTHER]
                ? this.props.customCategory.validation.text
                : ''
            }
            value   = { this.props.customCategory.value }
            onChange  = { (value) => { this.props.onChange('customCategory', value) } }
            maxLength = { 30 }
            disabled  = { !this.props.categories[this.props.OTHER] }
          />
        ]
      }
    </section>
  }
}

import React, { Component } from 'react'

import Dialog from 'react-toolbox/lib/dialog'
import Input from 'react-toolbox/lib/input'
import { RadioGroup, RadioButton } from 'react-toolbox/lib/radio'
import validator from 'validator'

import style from './style.scss'
import radioStyle from 'styles/ui/radioTheme.scss'

export default class RaceFormats extends Component {
  constructor(props) {
    super(props);

    this.getActiveFormat  = this.getActiveFormat.bind(this)
  }

  onFormatChange(value) {
    const formats      = this.props.formats
    const activeFormat = this.getActiveFormat()
    const customFormat = ''

    if(activeFormat !== value) {
      for(let format in formats) {
        if(format !== 'vaildation') {
          formats[format] = false

          if(format === value) {
            formats[format] = true
          }  
        }
      }

      this.props.onFormatChange({ formats, customFormat })
    }
  }

  getActiveFormat() {
    for(let format in this.props.formats) {
      if(format !== 'validation' && this.props.formats[format]) {
        return format
      }
    }
  }

  render() {
    const activeFormat = this.getActiveFormat()
    let formats = []

    for(let format in this.props.formats) {
      if(format !== 'validation') {
        formats.push(
          <RadioButton
            theme     = { radioStyle }
            label     = { format }
            value     = { format }
          />
        )  
      }
    }

    return <section>
      <span>
        Select race format
        <span className = { style.selectError }>
          {
            !this.props.formats[this.props.OTHER]
              ? this.props.formats.validation.text
              : ''
          }
        </span>
      </span>
      <RadioGroup
        key = 'radioGroup1'
        name = 'format'
        value = { activeFormat }
        onChange = {
          (value) => {
            this.onFormatChange(value)
          }
        }
      >
        { formats }
      </RadioGroup>
      <Input
        key = 'input1'
        type  = 'text'
        hint  = 'provide your own format'
        name  = 'customFormat'
        onBlur  = { () => { this.props.onBlur('customFormat') } }
        error   = {
            this.props.formats[this.props.OTHER]
              ? this.props.customFormat.validation.text
              : ''
         }
        value   = { this.props.customFormat.value }
        onChange  = { (value) => { this.props.onChange('customFormat', value) } }
        maxLength = { 30 }
        disabled  = { activeFormat !== this.props.OTHER }
      />
    </section>
  }
}
import React, { Component } from 'react'

import validator from 'validator'
import DatePicker from 'react-toolbox/lib/date_picker'

import style from './style.scss'

export default class Datepicker extends Component {
  render() {
    return (
      <div>
        <div>
          <span>
            Start date
            <span className = { style.selectError }>
              { this.props.startDate.validation.text }
            </span>
          </span>
          <DatePicker
            className = { style.datepicker }
            onDismiss = { () => { this.props.onBlur('startDate') } }
            onChange  = { (value) => { this.props.onChange('startDate', value) } }
            value     = { this.props.startDate.value } 
            onEscKeyDown    = { () => { this.props.onBlur('startDate') } }
            onOverlayClick  = { () => { this.props.onBlur('startDate') } }
          />
        </div>
        <div>
          <span>{ 'End date (if different than start date)' }</span>
          <DatePicker
            className = { style.datepicker }
            onChange  = { (value) => { this.props.onChange('endDate', value) } }
            value     = { this.props.endDate.value } 
            minDate   = { this.props.startDate.value }
          />
        </div>
      </div>
    )
  }
}
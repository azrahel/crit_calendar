import React, { Component } from 'react'

import Input from 'react-toolbox/lib/input'
import Checkbox from 'react-toolbox/lib/checkbox'
import Select from 'react-select'
import validator from 'validator'
import countries  from 'countries-cities'
import cities     from 'countries-cities'

import style from './style.scss'

import geodata from 'countries-list/countries.json'

export default class GeoSection extends Component {
  constructor(props) {
    super(props);

    this.getCountries = this.getCountries.bind(this)
  }

  onCustomCityChange(value) {
    this.props.onChange('city', value, value)
  }

  formatForSelect(toBeSelectable, subkey) {
    let selectable = []

    if(toBeSelectable.constructor === Array) {
      toBeSelectable.forEach((item) => {
        selectable.push({
          value: item,
          label: item
        })
      })
    } else {
      for(let key in toBeSelectable) {
        selectable.push({
          value: key,
          label: toBeSelectable[key][subkey] || toBeSelectable[key]
        })
      }  
    }

    return selectable
  }

  getCountries(countries) {
    let filteredCountries = {};
    let filtered = false

    if(this.props.continent.value) {
      filtered = true

      for(let country in countries) {
        if(countries[country].continent === this.props.continent.value.value) {
          filteredCountries[country] = countries[country];
        }
      }  
    }

    return filtered ? filteredCountries : countries;
  }

  render() {
    let choosableCities = this.props.country.value && this.props.country.value.label
      ? cities.getCities(this.props.country.value.label)
      : null;
    
    return <section>
      <div>
        <span>
          Continent
          <span className = { style.selectError }>
            { this.props.continent.validation.text }
          </span>
        </span>
        <Select
          name        = 'continent-select'
          placeholder = 'pick one'
          value       = { this.props.continent.value }
          onBlur      = { () => { this.props.onBlur('continent') } }
          options     = { this.formatForSelect(geodata.continents) }
          onChange    = { (value) => { this.props.onChange('continent', value) } }
        />
      </div>
      <div>
        <span>
          Country
          <span className = { style.selectError }>
            { this.props.country.validation.text }
          </span>
        </span>
        <Select
          name        = 'country-select'
          placeholder = 'pick one'
          value       = { this.props.country.value }
          options     = { this.formatForSelect(this.getCountries(geodata.countries), 'name') }
          onChange    = { (value) => { this.props.onChange('country', value) } }
          onBlur      = { () => { this.props.onBlur('country') } }
          disabled    = {
            !this.props.continent.value
          }
        />
      </div>
      <div>
        <span>
          City
          <span className = { style.selectError }>
            { !this.props.provideCustomCity ? this.props.city.validation.text : '' }
          </span>
        </span>
        <Select
          name        = 'city-select'
          placeholder = 'pick one'
          value       = { this.props.city.value }
          options     = { choosableCities ? this.formatForSelect(choosableCities) : null }
          onChange    = { (value) => { this.props.onChange('city', value) } }
          onBlur      = { () => { this.props.onBlur('city') } }
          disabled    = {
            !this.props.country.value ||
            !choosableCities ||
            this.props.provideCustomCity
          }
        />
      </div>
      <div>
        <label>
          <input
            type      = { 'checkbox' }
            className = { style.justChecking }
            checked   = { this.props.provideCustomCity }
            onChange  = { () => {
              this.props.onCustomCityCheckboxChange() }
            }
            disabled  = {
              !this.props.country
            }
          />  
          <span>My city is not provided</span>
        </label>
        <Input
          type      = 'text'
          name      = 'name'
          value     = { this.props.provideCustomCity ? this.props.city.value : '' }
          onChange  = { (value) => { this.onCustomCityChange(value) } }
          onBlur    = { () => { this.props.onBlur('city') } }
          error     = { this.props.provideCustomCity ? this.props.city.validation.text : '' }
          maxLength = { 30 }
          disabled  = { !this.props.provideCustomCity }
        />
      </div>
    </section>
  }
}


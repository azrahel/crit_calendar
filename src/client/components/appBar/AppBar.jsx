import React, { Component } from 'react'
import Dropdown from 'react-toolbox/lib/dropdown'
import localStorage from 'store'
import { AppBar as RTAppBar } from 'react-toolbox/lib/app_bar'
import Navigation from 'react-toolbox/lib/navigation'
import { Link as RTLink } from 'react-toolbox/lib/Link'
import EventDialog from 'components/dialog/Dialog'
import Button from 'react-toolbox/lib/button'
import classnames from 'classnames'

const interfaces = [
  { value: 'map', label: 'Map'},
  { value: 'calendar', label: 'Calendar' },
  { value: 'list', label: 'List' }
]

import style from './style.scss';

export default class AppBar extends Component {
  constructor(props) {
    super(props)

    let userInterfacePreference

    if(localStorage.get('critcalendar')) {
      userInterfacePreference = localStorage.get('critcalendar').interfaceType
    } else {
      let localStorageInterface = { interfaceType: interfaces[0].value }
      localStorage.set('critcalendar', localStorageInterface)

      userInterfacePreference = interfaces[0].value 
    }

    this.state = {
      interfaceType: userInterfacePreference
    }

    this.handleInterfaceChange = this.handleInterfaceChange.bind(this)
  }

  handleInterfaceChange(value, e) {
    localStorage.set('critcalendar', { interfaceType: value })

    this.props.handleInterfaceChange(value)
    
    e.preventDefault()
    e.stopPropagation()

    return false
  }

  render() {
    return (
      <RTAppBar className = { style.appBar }>
        <Navigation type = 'horizontal'>
          <RTLink href = '/' label = 'Cycling calendar'/>
          <Button onClick = { this.props.toggleDialog }>
            Add event
          </Button>
          {
            <Dropdown
              className = {
                classnames(
                  style.interfaceTypeChooser,
                  style.dropdown
                )
              }
              auto
              onChange = { this.handleInterfaceChange }
              source   = { interfaces }
              value    = { this.state.interfaceType }
            />
          }
        </Navigation>
      </RTAppBar>
    )
  }
}


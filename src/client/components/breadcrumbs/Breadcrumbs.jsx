import React, { Component } from 'react'
import { Link as RTLink } from 'react-toolbox/lib/Link'

import style from './style.scss';

export default class Breadcrumbs extends Component {
  render() {
    let crumbs = ['/']

    for(let location in this.props.params) {
      crumbs.push(this.props.params[location] + '/')
    }

    return (
      <div className = { style.breadcrumbsContainer }>
        CURRENT LOCATION: 
        {
          crumbs.map((crumb, index) => {
            let crumbUrl = crumbs.slice(0, index + 1).join('')
            
            return index !== crumbs.length - 1
              ? <RTLink href = { crumbUrl } label = { crumb }/>
              : <span>{ crumb }</span>
          })
        }
      </div>
    )
  }
}

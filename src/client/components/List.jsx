import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { render } from 'react-dom';
import moment from 'moment';
import 'moment/locale/en-gb';

import { connect } from 'react-redux'

import * as EventActions from '../actions/event'
import * as AreaFiltersActions from '../actions/areaFilters'

import BigCalendar from 'react-big-calendar';
BigCalendar.momentLocalizer(moment);

import { 
  getEvents, 
  getEventsForContinent, 
  getEventsForCountry, 
  getEventsForCity 
} from '../dbHandlers/events';

import style from './list.scss'

import { formatDBEventToCalendarEvent } from '../lib/functions';

const DNA = 'Data not available'

export default class List extends Component {
  constructor(props) {
    super(props);
  }

  getWorldEvents() {
    let startDate, endDate;
    
    if(this.props.timeSpan === 'day') {
      startDate = this.props.location.query.day;
      endDate = this.props.location.query.day;
    }
    if(this.props.timeSpan === 'month') {
      startDate = this.props.location.query.month + '-1';
      endDate = this.props.location.query.month + '-30';  //TODO: year-month should be checked
                          //and what is added to month string below should be defined this way
                          // (there is different quantity of days in months in different years)
    }
    if(this.props.timeSpan === 'year') {
      startDate = this.props.location.query.year + '-1-1';
      endDate = this.props.location.query.year + '-12-30';
    }

    getEvents(
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getContinentEvents(continent) {
    // getEventsForContinent(
    //   continent, 
    //   this.props.startDate, 
    //   this.props.endDate
    // )
    // .on('data', function(events) {
    //   this.setState({
    //     events: JSON.parse(events)
    //   });
    // }.bind(this));

    this.props.getContinentEvents(
      continent, 
      this.props.startDate, 
      this.props.endDate
    )
  }

  getCountryEvents(continent, country) {
    let startDate;
    let endDate;

    //TODO: date should be somehow validated somewhere
    if(this.props.timeSpan === 'day') {
      startDate = this.props.location.query.day;
      endDate = this.props.location.query.day;
    }
    if(this.props.timeSpan === 'month') {
      startDate = this.props.location.query.month + '-1';
      endDate = this.props.location.query.month + '-30';  //TODO: year-month should be checked
                          //and what is added to month string below should be defined this way
                          // (there is different quantity of days in months in different years)
    }
    if(this.props.timeSpan === 'year') {
      startDate = this.props.location.query.year + '-1-1';
      endDate = this.props.location.query.month + '-12-30';
    }

    getEventsForCountry(
      continent, 
      country,
      startDate, 
      endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getCityEvents(continent, country, city) {
    getEventsForCity(
      continent, 
      country,
      city,
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getEvents() {
    if(this.props.area) {
      if(this.props.area.continent) {
        if(this.props.area.country) {
          if(this.props.area.city) {
            this.getCityEvents(
              this.props.area.continent, 
              this.props.area.country,
              this.props.area.city,
            );

            return;
          }

          this.getCountryEvents(
            this.props.area.continent, 
            this.props.area.country
          );

          return;
        }

        this.getContinentEvents(this.props.area.continent)

        return;
      }
    }
    else {
      this.getWorldEvents()
    }
  }

  componentDidMount() {
    this.getEvents();
  }

  toggleHeight(e) {
    let animationTarget = e.currentTarget.parentNode
    let isActive = animationTarget.className.indexOf('active') > -1
    let chlidrenQuantity = animationTarget.childNodes.length + 1

    animationTarget.classList.toggle('active')

    if(animationTarget.className.indexOf('country') > -1) {
      if(!isActive) {
        animationTarget.style.maxHeight = (chlidrenQuantity * 200) + 'px';
        animationTarget.style.overflow = 'auto';
      } else {
        animationTarget.style.maxHeight = '30px';
        animationTarget.style.overflow = 'hidden';
      }
    } else if(animationTarget.className.indexOf('city') > -1) {
      if(!isActive) {
        animationTarget.style.maxHeight = (chlidrenQuantity * 200) + 'px';
      } else {
        animationTarget.style.maxHeight = '28px';
      }
    }

    e.preventDefault()
    e.stopPropagation()
    e.stopPropagation()

    return false
  }

  followUrl(e, url) {
    window.open(url,'_blank');

    e.preventDefault()
    e.stopPropagation()
    e.stopPropagation()

    return false
  }

  renderCitiesEvents(countryEvents) {
    function renderCityEvents(city, countryEvents) {
      function renderEvent(event) {
        event = formatDBEventToCalendarEvent(event)

        let startDate = event.start.toLocaleDateString('en-GB', {  
            day : 'numeric',
            month : 'short',
            year : 'numeric'
        }).split(' ').join('-');

        let endDate = event.end.toLocaleDateString('en-GB', {  
            day : 'numeric',
            month : 'short',
            year : 'numeric'
        }).split(' ').join('-');

        return <div className = { style.eventContainer }>
          <div className = { style.name }>
            <span>Name: </span>
            <span>{ event.title }</span>
          </div>
          <div className = { style.date }>
            <span>Date: </span>
            <span>
            {
              event.allDay
                ? <span>{ startDate }</span>
                : <span>{ startDate + '-' + endDate }</span>
            }
            </span>
          </div>
          <div className = { style.name }>
            <span>Allowed bicycles: </span>
            <span>
              {
                event.categories
                  ? event.categories.join(',')
                  : DNA
              }
            </span>
          </div>
          <div className = { style.name }>
            <span>Organizer: </span>
            <span>
              { event.organizer || DNA }
            </span>
          </div>
          <div className = { style.url }>
            <span>Link: </span>
            <a 
              onClick = {
                (e) => {
                  this.followUrl(e, event.url || DNA) 
                }
              }
              className = { style.fbUrl }
            >
              <span>{ 'EVENT PAGE' }</span>
            </a>
          </div>  
        </div>
      }

      let eventsQuantityInCity = 0

      for(let city in countryEvents) {
        eventsQuantityInCity += countryEvents[city].length
      }

      return (
        <div className = { style.city }>
          <header className = { style.header } onClick = { (e) => { this.toggleHeight(e) } }>
            City:
            <span>{ city }</span>
            <span>{ '(' + eventsQuantityInCity + ')' }</span>
          </header>
          <div className = { style.eventsList }>
            {
              countryEvents[city].map((event) => {
                return renderEvent.bind(this)(event)
              })
            }
          </div>
        </div>
      )
    }

    let citiesEvents = []

    for(let city in countryEvents) {
      citiesEvents.push(
        renderCityEvents.bind(this)(city, countryEvents)
      )
    }

    return citiesEvents
  }

  renderCountriesEvents(eventsByCountries) {
    let countriesEvents = []

    for(let country in eventsByCountries) {
      let eventsQuantityInCountry = 0

      for(let city in eventsByCountries[country]) {
        eventsQuantityInCountry += eventsByCountries[country][city].length
      }


      countriesEvents.push(
        <div className = { style.country }>
          <header className = { style.header } onClick = { (e) => { this.toggleHeight(e) } }>
            Country:
            <span>{ country }</span>
            <span>{ '(' + eventsQuantityInCountry + ')' }</span>
          </header>
          <div className = { style.citiesList }>
            {
              this.renderCitiesEvents(eventsByCountries[country])
            }
          </div>
        </div>
      )
    }

    return countriesEvents
  }

  render() {
    console.log('this.props.events')
    console.log(this.props.events)
    return (
      <div className = { style.countriesList }>
        { this.renderCountriesEvents(this.props.events) }
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log('ownProps')
  console.log(ownProps)
  return {
    events: state.events
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getContinentEvents: (continent, startDate, endDate) => {
      dispatch(EventActions.fetchContinentEvents(continent, startDate, endDate))
    },
    onContinentClick: (continentShortName) => {
      dispatch(
        AreaFiltersActions.setAreaFilterAction({
          continent: continentShortName
        })
      )

      let fullContinentName = continents.filter(function(continent) {
        return continent.short.toLowerCase() === continentShortName
      })[0].full

      browserHistory.push(fullContinentName)
    }
  }
}

const ListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(List)


export default ListContainer
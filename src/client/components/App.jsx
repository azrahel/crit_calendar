import React, { Component } from 'react'
import Calendar from './Calendar.jsx'
import List from './List.jsx'
import MapView from './Map.jsx'
import Breadcrumbs from 'components/breadcrumbs/Breadcrumbs'
import { getEvents } from '../dbHandlers/events'
import { formatDBEventToCalendarEvent } from '../lib/functions'

import Dropdown from 'react-toolbox/lib/dropdown'
import localStorage from 'store'

import AppBar from 'components/appBar/AppBar'
import Navigation from 'react-toolbox/lib/navigation'
import { Link as RTLink } from 'react-toolbox/lib/Link'
import EventDialog from 'components/dialog/Dialog'
import Button from 'react-toolbox/lib/button'

const interfaces = [
  { value: 'map', label: 'Map'},
  { value: 'calendar', label: 'Calendar' },
  { value: 'list', label: 'List' }
]

import style from 'styles/index.scss';

export default class App extends Component {
  constructor(props) {
    super(props)

    let userInterfacePreference

    if(localStorage.get('critcalendar')) {
      userInterfacePreference = localStorage.get('critcalendar').interfaceType
    } else {
      let localStorageInterface = { interfaceType: interfaces[0].value }
      localStorage.set('critcalendar', localStorageInterface)

      userInterfacePreference = interfaces[0].value 
    }

    this.state = {
      interfaceType: userInterfacePreference
    }

    this.toggleDialog                   = this.toggleDialog.bind(this)
    this.handleDropdownInterfaceChange  = this.handleDropdownInterfaceChange.bind(this)
  }

  renderBody() {
    let chosenInterface

    // let timeSpan = Object.keys(this.props.location.query)[0]

    switch(this.state.interfaceType) {
      case 'map':
        chosenInterface = <MapView />

        break
      case 'calendar':
        chosenInterface = <Calendar
          startDate = { this.props.location.query.startDate }
          endDate = { this.props.location.query.endDate }
        />

        break
      case 'calendar':
        chosenInterface = <List 
          { ...this.props } 
          timeSpan = { timeSpan }
        />

        break
    }

    return chosenInterface
  }

  handleDropdownInterfaceChange(value) {
    this.setState({ interfaceType: value })
  }

  toggleDialog() {
    this.setState({
      dialogActive: !this.state.dialogActive
    })
  }

  render() {
    return (
      <div className = { style.appContainer }>
        <AppBar
          handleInterfaceChange = { this.handleDropdownInterfaceChange }
          toggleDialog = { this.toggleDialog }
        />
        <EventDialog
          toggleDialog = { this.toggleDialog }
          active = { this.state.dialogActive }
        />
        <div className = { style.appBody }>
          <Breadcrumbs params = { this.props.params }/>
          { this.props.children || this.renderBody() }  
        </div>
      </div>
    )
  }
}

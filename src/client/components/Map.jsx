import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { render } from 'react-dom'
import { connect } from 'react-redux'
import { 
  getEvents, 
  getEventsForContinent, 
  getEventsForCountry, 
  getEventsForCity 
} from '../dbHandlers/events'

import * as EventActions from '../actions/event'
import * as AreaFiltersActions from '../actions/areaFilters'

import { formatDBEventToCalendarEvent } from '../lib/functions'

import WorldMap from 'react-world-map'

import style from './map.scss'


const continents = [
  { short: 'NA', numberOnMapPositionLeft: 140, numberOnMapPositionTop: 95 },
  { short: 'SA', numberOnMapPositionLeft: 235, numberOnMapPositionTop: 315 },
  { short: 'AF', numberOnMapPositionLeft: 490, numberOnMapPositionTop: 225 },
  { short: 'EU', numberOnMapPositionLeft: 510, numberOnMapPositionTop: 70 },
  { short: 'AS', numberOnMapPositionLeft: 670, numberOnMapPositionTop: 100 },
  { short: 'OC', numberOnMapPositionLeft: 850, numberOnMapPositionTop: 350 },
]

class MapView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      events: []
    }
  }

  componentDidMount() {
    window.addEventListener('WorldMapClicked', function(e) {
      if(e.detail.clickedState !== 'none') {
        this.props.onContinentClick(
          continents,
          e.detail.clickedState
        )
      }
    }.bind(this))
     

    this.props.getEventsCount()
  }

  componentDidUpdate() {
    this.setMap()
  }

  componentWillUnmount() {
    window.removeEventListener('WorldMapClicked', null)
  }

  setMap() {
    let mapContainer = document.getElementsByClassName('row')[0]

    continents.forEach((continent, index) => {
      //position events number on continent shape
      let left = document.getElementById(continent.short).getBoundingClientRect().left
      let width = document.getElementById(continent.short).getBoundingClientRect().width

      let top = document.getElementById(continent.short).getBoundingClientRect().top
      let height = document.getElementById(continent.short).getBoundingClientRect().height

      var span = document.createElement('span')
      
      span.innerHTML = this.props.events[continent.short].eventsQuantity
      
      span.style.color = 'red'
      span.style['font-size'] = '25px'
      span.style.position = 'absolute'
      span.style.top = continent.numberOnMapPositionTop + 'px'
      span.style.left = continent.numberOnMapPositionLeft + 'px'

      mapContainer.appendChild(span)
    }) 
  }

  getWorldEvents() {
    let startDate, endDate
    
    if(this.props.timeSpan === 'day') {
      startDate = this.props.location.query.day
      endDate = this.props.location.query.day
    }
    if(this.props.timeSpan === 'month') {
      startDate = this.props.location.query.month + '-1'
      endDate = this.props.location.query.month + '-30'  //TODO: year-month should be checked
                          //and what is added to month string below should be defined this way
                          // (there is different quantity of days in months in different years)
    }
    if(this.props.timeSpan === 'year') {
      startDate = this.props.location.query.year + '-1-1'
      endDate = this.props.location.query.year + '-12-30'
    }


    // getEvents(
    //   this.props.startDate, 
    //   this.props.endDate
    // ).then((events) => {
    //   this.setState({
    //     events: events
    //   })
    // })
  }

  getContinentEvents(continent) {
    getEventsForContinent(
      continent, 
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      })
    }.bind(this))
  }

  getCountryEvents(continent, country) {
    let startDate
    let endDate

    //TODO: date should be somehow validated somewhere
    if(this.props.timeSpan === 'day') {
      startDate = this.props.location.query.day
      endDate = this.props.location.query.day
    }
    if(this.props.timeSpan === 'month') {
      startDate = this.props.location.query.month + '-1'
      endDate = this.props.location.query.month + '-30'  //TODO: year-month should be checked
                          //and what is added to month string below should be defined this way
                          // (there is different quantity of days in months in different years)
    }
    if(this.props.timeSpan === 'year') {
      startDate = this.props.location.query.year + '-1-1'
      endDate = this.props.location.query.month + '-12-30'
    }

    getEventsForCountry(
      continent, 
      country,
      startDate, 
      endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      })
    }.bind(this))
  }

  getCityEvents(continent, country, city) {
    getEventsForCity(
      continent, 
      country,
      city,
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      })
    }.bind(this))
  }

  getEvents() {
    if(this.props.area) {
      if(this.props.area.continent) {
        if(this.props.area.country) {
          if(this.props.area.city) {
            this.getCityEvents(
              this.props.area.continent, 
              this.props.area.country,
              this.props.area.city,
            )

            return
          }

          this.getCountryEvents(
            this.props.area.continent, 
            this.props.area.country
          )

          return
        }

        this.getContinentEvents(this.props.area.continent)

        return
      }
    }
    else {
      this.getWorldEvents()
    }
  }

  render() {
    return <div className = { style.map }>
      <WorldMap style/>
    </div>
  }
}

const mapStateToProps = (state) => {
  return {
    events: state.events
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getEventsCount: (startDate, endDate) => {
      dispatch(EventActions.fetchEventsCount(startDate, endDate))
    },
    onContinentClick: (continents, continentShortName) => {
      dispatch(
        AreaFiltersActions.setAreaFilterAction({
          continent: continentShortName
        })
      )

      let continentName = continents.filter(function(continent) {
        return continent.short.toLowerCase() === continentShortName
      })[0].short

      browserHistory.push(continentName)
    }
  }
}

const MapViewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MapView)


export default MapViewContainer



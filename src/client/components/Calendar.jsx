import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { render } from 'react-dom';
import moment from 'moment';
import 'moment/locale/en-gb';

import BigCalendar from 'react-big-calendar';
BigCalendar.momentLocalizer(moment);

import { 
  getEvents, 
  getEventsForContinent, 
  getEventsForCountry, 
  getEventsForCity 
} from '../dbHandlers/events';
import { formatDBEventToCalendarEvent } from '../lib/functions';

export default class Calendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      events: []
    };
  }

  monthCellRender(data) {
    return <div>{ months[data.fields[2]] }</div>
  }

  navigateToEvent(event) {
    console.log('event nagivation triggered!');
    console.log(event);
  }

  onView(data, e) {
    console.log('view changed')
    console.log(data);
    console.log(e);
  }

  onSelectSlot(data) {
    let day = data.start.getDate();
    let month = data.start.getMonth();
    let year = data.start.getFullYear();

    let urlDate =  year + '-' + (month + 1) + '-' + day;

    browserHistory.push('?day=' + urlDate)
  }

  getWorldEvents() {
    getEvents(
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getContinentEvents(continent) {
    getEventsForContinent(
      continent, 
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getCountryEvents(continent, country) {
    getEventsForCountry(
      continent, 
      country,
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getCityEvents(continent, country, city) {
    getEventsForCity(
      continent, 
      country,
      city,
      this.props.startDate, 
      this.props.endDate
    )
    .on('data', function(events) {
      this.setState({
        events: JSON.parse(events)
      });
    }.bind(this));
  }

  getEvents() {
    if(this.props.area) {
      if(this.props.area.continent) {
        if(this.props.area.country) {
          if(this.props.area.city) {
            this.getCityEvents(
              this.props.area.continent, 
              this.props.area.country,
              this.props.area.city,
            );

            return;
          }

          this.getCountryEvents(
            this.props.area.continent, 
            this.props.area.country
          );

          return;
        }

        this.getContinentEvents(this.props.area.continent)

        return;
      }
    }
    else {
      this.getWorldEvents()
    }
  }

  componentDidMount() {
    this.getEvents();
  }

  render() {
    let events = this.state.events.map(function(event) {
      return formatDBEventToCalendarEvent(event);
    });

    return (
      <div { ...this.props } style={{ height: '97vh' }}>
        <BigCalendar
          selectable
          views         = { ['month'] }
          events        = { events }
          onView        = { this.onView.bind(this) }
          onSelectEvent = { this.navigateToEvent.bind(this) }
          onSelectSlot  = { this.onSelectSlot.bind(this) }
          startDate     = { new Date(this.props.startDate) }
        />
      </div>
    );
  }
}
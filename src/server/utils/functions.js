
module.exports = {
  sortEvents: function (field, events) {
    events.sort((eventA, eventB) => {
      return (eventA[field] < eventB[field]
        ? -1
        : (eventA[field] > eventB[field])
          ? 1
          : 0
      )
    })
  },

  groupEventsByContinents: function(eventsSortedByContinents) {
    let groupedEvents = {}

    eventsSortedByContinents.forEach((event) => {
      if(groupedEvents[event.continent]) {
        groupedEvents[event.continent].push(event)
      } else {
        groupedEvents[event.continent] = [event]
      }
    })

    return groupedEvents
  },

  groupEventsByCountries: function(eventsSortedByCountries) {
    let groupedEvents = {}

    eventsSortedByCountries.forEach((event) => {
      if(groupedEvents[event.country]) {
        groupedEvents[event.country].push(event)
      } else {
        groupedEvents[event.country] = [event]
      }
    })

    return groupedEvents
  },
  
  //TO BE CHANGED i wtedy wszystkie te trzy funkcje można ujednolicić
  groupCountryEventsByCities: function(eventsGroupedByCountry) {
    for(let country in eventsGroupedByCountry) {
      let ungroupedCountryEvents = eventsGroupedByCountry[country]
      let countryEventsGroupedByCities = {}

      ungroupedCountryEvents.forEach((event) => {
        if(countryEventsGroupedByCities[event.city]) {
          countryEventsGroupedByCities[event.city].push(event)
        } else {
          countryEventsGroupedByCities[event.city] = [event]
        }
      })

      eventsGroupedByCountry[country] = countryEventsGroupedByCities
    }

    return eventsGroupedByCountry;
  }
}

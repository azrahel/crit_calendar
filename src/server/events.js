/*jshint esnext: true */

var express       = require('express');
// var passport      = require('passport');
// var LocalStrategy = require('passport-local').Strategy;
var bodyParser    = require('body-parser');
var cookieParser  = require('cookie-parser');
var session       = require('express-session');
var MongoStore    = require('connect-mongo')(session);
var ObjectId      = require('mongojs').ObjectId;
// var bcrypt        = require('bcrypt-nodejs');
// var moment        = require('moment');
// var nodemailer    = require('nodemailer');
// var sgTransport   = require('nodemailer-sendgrid-transport');
// var crypto        = require('crypto');

let apiVersion = '/v1'

var functions = require('./utils/functions')

module.exports = function(params) {
  var app             = params.app;
  var db              = params.db;
  var eventsCollection = db.collection('events');

  function limitSearchToFuture(req, res, next) {
    let appName = 'critCalendar';
    let apiNo = apiVersion.slice(1);

    let currentYear = new Date().getFullYear();
    let today = new Date();
    let firstDayOfNextYear = new Date(currentYear - 1, 11, 31);

    let currentYearOnly = { ISOStartDate: {
        $gte: today,
        $lt: firstDayOfNextYear
      }
    }

    req[appName][apiNo].queryLimiter = currentYearOnly

    next();
  }

  function injectParamsIntoQueryObject(req, res, next) {
    let appName = 'critCalendar';
    let apiNo = apiVersion.slice(1);

    let appMiddlewareValuesStore = {};
    appMiddlewareValuesStore[apiNo] = {};

    Object.keys(req.query).map(function(key, index) {
      req[appName][apiNo][key] = req.query[key];
    });

    next();
  }

  function addAppMiddlewareValuesStoreToRequest(req, res, next) {
    //TODO move to config;
    let appName = 'critCalendar';
    let apiNo = apiVersion.slice(1);

    if(!req.critCalendar) {
      req['critCalendar'] = {};
      req['critCalendar'][apiNo] = {};  
    }
    
    next();
  }

  app.get(apiVersion + '/events',
    addAppMiddlewareValuesStoreToRequest,
    limitSearchToFuture,
    injectParamsIntoQueryObject,
    function(req, res, next) {
    //TODO: ISOStartDate must equal startDate and ISOEndDate muse equal endDate
    //how to create ISODate in js
    //and than how to compare ISODates in Mongo
    eventsCollection.find({ 
      // ISOStartDate: new Date(req.query.startDate)
      // ISOEndDate: new Date(req.query.endDate)
    }, function(error, documents) {
      res.json(documents);
    });  
  });

  app.get(apiVersion + '/count/events',
    addAppMiddlewareValuesStoreToRequest,
    limitSearchToFuture,
    injectParamsIntoQueryObject,
    function(req, res, next) {
    //TODO: ISOStartDate must equal startDate and ISOEndDate muse equal endDate
    //how to create ISODate in js
    //and than how to compare ISODates in Mongo
    eventsCollection.find({ 
      // ISOStartDate: new Date(req.query.startDate)
      // ISOEndDate: new Date(req.query.endDate)
    }, function(error, documents) {
      functions.sortEvents('continent', documents)
      let eventsGroupedByContinents = functions.groupEventsByContinents(documents)

      for(let continent in eventsGroupedByContinents) {
        eventsGroupedByContinents[continent] = { eventsQuantity: eventsGroupedByContinents[continent].length }
      }

      res.json(eventsGroupedByContinents);
    });  
  });



  app.get(apiVersion + '/events/country/:country',
    addAppMiddlewareValuesStoreToRequest,
    limitSearchToFuture,
    injectParamsIntoQueryObject,
    function(req, res, next) {

    let country = req.params.country;

    eventsCollection.find({ country: country }, function(error, documents) {
      res.json(documents);
    });  
  });

  app.get(apiVersion + '/events/continent/:continent',
    addAppMiddlewareValuesStoreToRequest,
    limitSearchToFuture,
    injectParamsIntoQueryObject,
    function(req, res, next) {

    let continent = req.params.continent;
    let queryObject = { continent: continent };

    if(!(req.query.startDate && req.query.endDate)) {
      // console.log('currentYearOnly')
      // let currentYear = new Date().getFullYear();

      // let firstJanuaryCurrentYear = new Date(currentYear, 0, 1);
      // let lastDecemberCurrentYear = new Date(currentYear, 11, 31);

      // queryObject.unixStartDate = {
      //   $gte: firstJanuaryCurrentYear,
      //   $lt:  lastDecemberCurrentYear
      // }  
    }
    
    eventsCollection.find(queryObject, function(error, documents) {
      functions.sortEvents('country', documents)

      let eventsByCountries = functions.groupCountryEventsByCities(
          functions.groupEventsByCountries(documents)
        )

      res.json(eventsByCountries);
    });  
  });

  app.post(apiVersion + '/events/add',
    addAppMiddlewareValuesStoreToRequest,
    limitSearchToFuture,
    injectParamsIntoQueryObject,
    function(req, res, next) {
      if (req.method === 'POST') {
          eventsCollection.insert(
            JSON.parse(JSON.stringify(req.body)),

            function(error, document) {
              if (error) {
                res.status(500).send(error);
              }
              else {
                res.json({
                  value: 'Event has been added to the moderation queue',
                  success: true
                });
              }
            }
          )
      }
    }
  )
};
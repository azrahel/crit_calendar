var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var session       = require('express-session');
// var LocalStrategy = require('passport-local').Strategy;
// var passport      = require('passport');
var MongoStore    = require('connect-mongo')(session);
var mongojs       = require('mongojs');
var ObjectId      = require('mongojs').ObjectId;
var mongodb       = mongojs('critcalendar');
var app           = express();
// var auth          = require('server/auth')(mongodb);
var cors          = require('cors');


var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

var app = express();

var corsOptions = {
  origin: true,
  methods: ['GET', 'PUT', 'POST', 'DELETE'],
  credentials: true
};

app.use(cors(corsOptions));

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'src/client/public')));

var server = app.listen(3001, 'localhost', function(){
    var host = server.address().address;
    var port = server.address().port;
    console.log('Server app listening at http://%s:%s', host, port);
});

var params = {
    root: __dirname,
    app: app,
    db: mongodb
};

['events'].forEach(function(module, index) {
    require('./src/server/' + module)(params);
});

//webpack dev server configuration below

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
}).listen(3000, 'localhost', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Client app Listening at http://localhost:3000/');
});
